/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import IControl.Action;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Patri
 */
public class LogoutController implements Action {

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse res) {
        req.setAttribute("usuario", null);
        try{
            RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
            rd.forward(req,res);
        }catch(Exception e){
            e.getMessage();
        }
    }
    
}
