/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Dao.ModelDao;
import IControl.Action;
import Model.Usuario;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Patri
 */
public class Teste implements Action {

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse res) {
        ModelDao dao = new ModelDao();
        System.out.println(Usuario.class + " - Teste");
        List<Usuario> lista = dao.getList(Usuario.class.getSimpleName());
        if(lista != null){
            lista.forEach(c -> {
                System.out.println(c.getNome());
            });
        }
    }
}
