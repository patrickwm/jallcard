/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Dao.UsuarioDao;
import IControl.Action;
import Model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Patri
 */
public class LoginController implements Action {

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse res) {
        String email = req.getParameter("email");
        String senha = req.getParameter("senha");
        UsuarioDao dao = new UsuarioDao();
        Usuario user = dao.procuraLogin(email,senha);
        if(user != null){
            try {
                req.setAttribute("usuario", user);
                res.setStatus(200);
                res.getWriter().write("Inicial.jsp");
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
       } else {
            res.setContentType("text/html");
            res.setCharacterEncoding("UTF-8");
            res.setStatus(401);
            try {
                res.getWriter().write("Login Inválido");
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
}
