/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Connection.Hibernate;
import IModel.Model;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Patrick
 */
public class ModelDao {
    protected Hibernate conexao = new Hibernate();
    protected Session sessao;
    
    public ModelDao(){
        this.sessao = conexao.openSession();
    }
    
    public void add(Model a) throws Exception{
        try{
            Transaction tx = sessao.beginTransaction();
            sessao.save(a);
            tx.commit();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }finally{
            closeConnections();
        }
    }
    
    public boolean remove(Model a){
        try{
            Transaction tx = sessao.beginTransaction();
            sessao.delete(a);
            tx.commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            closeConnections();
        }
    }
    
    public boolean update(Model a){
        try{
            Transaction tx = sessao.beginTransaction();
            sessao.update(a);
            tx.commit();
            return true;
        }catch(Exception e){
            e.printStackTrace();    
            return false;
        }finally{
            closeConnections();
        }
    }
    
    public <T extends Model> List<T> getList(String nomeTabela){
        List<T> list;
        try{
            Transaction tx = sessao.beginTransaction();
            Query consulta = sessao.createQuery("from "+nomeTabela);
            list = consulta.list();
            tx.commit();
            return list;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            closeConnections();
        }
    }
    
    public void closeConnections(){
        this.sessao.close();
        this.conexao.closeSession();
    }
    
}
