/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Usuario;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Patri
 */
public class UsuarioDao extends ModelDao{
    public UsuarioDao(){
        super();
    }
    
    public Usuario procuraLogin(String email, String senha){
        Usuario u = null;
        try{
            Transaction tx = sessao.beginTransaction();
            Criteria consulta = sessao.createCriteria(Usuario.class)
            .add(Restrictions.eq("email", email))
            .add(Restrictions.eq("senha", senha))
            .add(Restrictions.eq("habilitado", true));
            u = (Usuario) consulta.uniqueResult();
            tx.commit();
            return u;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            closeConnections();
        }
    }
}
