/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Patrick
 */
public class Hibernate {
    public SessionFactory conexao = getConnection();
    
    public SessionFactory getConnection(){
        Configuration cfg = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(
        cfg.getProperties());
        SessionFactory sessao = cfg.buildSessionFactory(builder.build());
        return sessao;
    }
    
    public Session openSession(){
        return conexao.openSession();
    }
    
    public void closeSession(){
        conexao.close();
    }
}
