/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import IModel.Model;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import static javax.persistence.FetchType.EAGER;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Patri
 */
@Entity @Table
public class Usuario extends Model {
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    private String nome;
    private String email;
    private String senha;
    private boolean habilitado;
    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH},fetch = EAGER) 
    @JoinTable(
        name="usuario_permissao", 
        joinColumns = {@JoinColumn(name="id_usuario")}, 
        inverseJoinColumns = {@JoinColumn(name="id_permissao")}
    )
    private List<Permissao> permissoes;

    public Usuario(int id, String nome, String email, String senha, boolean habilitado) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.habilitado = habilitado;
    }

    public Usuario(String nome, String email, String senha, boolean habilitado) {
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.habilitado = habilitado;
    }

    public Usuario() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public List<Permissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<Permissao> permissoes) {
        this.permissoes = permissoes;
    }
    
}
