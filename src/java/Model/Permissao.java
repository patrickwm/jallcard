/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import IModel.Model;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import static javax.persistence.FetchType.EAGER;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Patri
 */
@Entity @Table
public class Permissao extends Model {
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    private String nome;
    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH},fetch = EAGER, mappedBy="permissoes")
    private List<Usuario> usuarios;

    public Permissao(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Permissao(String nome) {
        this.nome = nome;
    }
    
    public Permissao(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    
    
}
