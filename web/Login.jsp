<%-- 
    Document   : Login
    Created on : 05/03/2019, 20:54:29
    Author     : Patri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JallCard - Login</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="./CSS/padrao.css"> 
        <link rel="stylesheet" type="text/css" href="./CSS/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./CSS/bootstrap-reboot.css">
        <link rel="stylesheet" type="text/css" href="./CSS/bootstrap-grid.css">
        <script src="./JS/jquery-3.3.1.js"></script>
        <script src="./JS/jquery-latest.js"></script>
        <script>
            $(document).ready(function() {
                $('#LoginInvalido').hide();
                $('#senha').keydown(function(event) {
                    // enter has keyCode = 13, change it if you want to use another button
                    if (event.keyCode == 13) {
                       submitForm(event);
                    }
                });
                $('#submit').click(function(event) {
                    submitForm(event);
                });
                
                function submitForm(event){
                    let email = $('#email').val();
                    let senha = $('#senha').val();
                    $.get('Controlador?action=Login',{ email:email, senha:senha },function(response) {
                        window.location = response;
                    }).fail(function(response) {
                        $('#LoginInvalido').show();
                        $('#LoginInvalido').text(response.responseText);
                    });
                }
            });
        </script>
        
    </head>
    <body class="align">
        
        <div class="gridLogin align__item">

            <div class="register">

                <img src="./IMG/logo.png" class="site__logo">
                
                
                <div class="form">

                    <div class="form__field">
                        <input id="email" name="email" type="email" placeholder="Digite seu e-mail">
                    </div>

                    <div class="form__field">
                        <input id="senha" name="senha" type="password" placeholder="Digite sua senha">
                    </div>

                    <div class="form__field">
                        <input id="submit" type="submit" value="Entrar">
                    </div>
                    
                </div>
                <div id="LoginInvalido" class="alert alert-danger" role="alert"></div>
                <p>Não esta cadastrado? entre em contato conosco (XX) XXXX-XXXX</p>

            </div>

        </div>

    </body>
</html>
